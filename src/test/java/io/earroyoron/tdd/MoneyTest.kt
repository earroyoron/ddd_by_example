package io.earroyoron.tdd

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import java.math.BigDecimal

class MoneyTest {

    @DisplayName("Simply dollar multiplication")
    @Test
    fun testDollarMultiplication() {
        val fiveDollars = Dollar(5)
        Assertions.assertTrue(fiveDollars == Dollar(5))
    }

    data class Dollar(val amount: BigDecimal) {
        constructor(amount: Int) : this(BigDecimal(amount))
    }


}